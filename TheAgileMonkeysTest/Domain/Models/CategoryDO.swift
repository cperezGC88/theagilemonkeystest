//
//  CategoryDO.swift
//  TheAgileMonkeysTest
//
//  Created by Carlos Perez on 09/04/2019.
//  Copyright © 2019 Carlos Perez. All rights reserved.
//

class CategoryDO {

    let categoryId : String?
    let children : [CategoryDO]?
    let name : String?

    init(dto: Category) {
        self.categoryId = dto.categoryId
        self.children = dto.children?.map({CategoryDO(dto: $0)})
        self.name = dto.name
    }
}

extension CategoryDO: Equatable {
    static func == (lhs: CategoryDO, rhs: CategoryDO) -> Bool {
        return lhs.categoryId == rhs.categoryId
    }
}
