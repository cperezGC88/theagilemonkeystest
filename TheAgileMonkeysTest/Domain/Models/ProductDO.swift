//
//  ProductDO.swift
//  TheAgileMonkeysTest
//
//  Created by Carlos Perez on 10/04/2019.
//  Copyright © 2019 Carlos Perez. All rights reserved.
//

class ProductDO {

    let currency : CurrencyType?
    let finalPrice : Int?
    let images : [String]?
    let name : String?
    let originalPrice : Int?
    var categoryId: Int? = nil

    init(dto: Product) {
        self.currency = CurrencyType(rawValue: dto.currency ?? "")
        self.finalPrice = dto.finalPrice
        self.images = dto.images
        self.name = dto.name
        self.originalPrice = dto.originalPrice
    }
}
