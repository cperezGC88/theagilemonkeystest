//
//  StoreDO.swift
//  TheAgileMonkeysTest
//
//  Created by Carlos Perez on 09/04/2019.
//  Copyright © 2019 Carlos Perez. All rights reserved.
//

class StoreDO {

    let countryCode : String?
    let name : String?
    let storeCode : String?
    let storeViews : [StoreViewDO]?
    let websiteCode : String?

    init(dto: Store) {
        self.countryCode = dto.countryCode
        self.name = dto.name
        self.storeCode = dto.storeCode
        self.storeViews = dto.storeViews?.map({StoreViewDO(dto: $0)})
        self.websiteCode = dto.websiteCode
    }
}
