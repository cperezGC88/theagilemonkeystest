//
//  StoreViewDO.swift
//  TheAgileMonkeysTest
//
//  Created by Carlos Perez on 09/04/2019.
//  Copyright © 2019 Carlos Perez. All rights reserved.
//

class StoreViewDO {

    let name : String?
    let storeId : Int?

    init(dto: StoreView) {
        self.name = dto.name
        self.storeId = dto.storeId
    }
}
