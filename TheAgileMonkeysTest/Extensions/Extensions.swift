import Foundation
import UIKit
import CommonCrypto

enum TimeFormat: String {
    case HHmm = "HH:mm"
    case MMddYYYY = "MM/dd/YYYY"
    case YYYY_MM_ddHH_mm_ssZZZZ = "YYYY-MM-dd HH:mm:ss ZZZZ"
    case YYYYMMddHH_mmss = "MM/dd/YYYY HH:mm"
}

extension Dictionary {
    func toMutable() -> NSMutableDictionary {
        let keyDictionary = NSMutableDictionary()

        for (newKey, value) in self {
            keyDictionary.setValue(value, forKey: newKey as! String)
        }

        return keyDictionary
    }
}

extension Date {
    static var zero: Date {
        return Date(timeIntervalSince1970: 0)
    }

    static var tomorrow: Date {
        return Calendar.current.date(bySetting: .hour, value: 0, of: Date())!
    }

    func toString(format: TimeFormat? = nil) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = (format ?? .YYYY_MM_ddHH_mm_ssZZZZ).rawValue
        dateFormatter.timeZone = TimeZone.init(abbreviation: "GMT")
        return dateFormatter.string(from: self)
    }
}

extension String {

    func toDate(format: TimeFormat? = nil) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = (format ?? .YYYY_MM_ddHH_mm_ssZZZZ).rawValue
        dateFormatter.timeZone = TimeZone.init(abbreviation: "GMT")
        return dateFormatter.date(from: self)
    }

    func md5() -> String! {
        let str = self.cString(using: String.Encoding.utf8)
        let strLen = CUnsignedInt(self.lengthOfBytes(using: String.Encoding.utf8))
        let digestLength = Int(CC_MD5_DIGEST_LENGTH)
        let result = UnsafeMutablePointer<CUnsignedChar>.allocate(capacity: digestLength)

        CC_MD5(str!, strLen, result)

        let hash = NSMutableString()

        for i in 0..<digestLength {
            hash.appendFormat("%02x", result[i])
        }

        result.deinitialize()

        return String(format: hash as String)
    }
}

extension UITableView {
    func performTableUpdates(rowsToRemove: [IndexPath], sectionsToRemove: [Int], rowsToInsert: [IndexPath], sectionsToInsert: [Int], rowsToUpdate: [IndexPath], animationMode: UITableView.RowAnimation = .fade) {

        self.beginUpdates()

        if sectionsToRemove.count > 0 {
            self.deleteSections(IndexSet(sectionsToRemove), with: animationMode)
        }

        if rowsToRemove.count > 0 {
            let removedSections = sectionsToRemove
            let rowsToRemoveNotBelongingToRemovedSection = rowsToRemove.filter({!removedSections.contains($0.section)})
            self.deleteRows(at: rowsToRemoveNotBelongingToRemovedSection, with: animationMode)
        }

        if sectionsToInsert.count > 0 {
            self.insertSections(IndexSet(sectionsToInsert), with: animationMode)
        }

        if rowsToInsert.count > 0 {
            self.insertRows(at: rowsToInsert, with: animationMode)
        }

        if rowsToUpdate.count > 0 {
            self.reloadRows(at: rowsToUpdate, with: animationMode)
        }

        self.endUpdates()
    }
}

extension Array where Element : Equatable {
    var unique: [Element] {
        var uniqueValues: [Element] = []
        forEach { item in
            if !uniqueValues.contains(item) {
                uniqueValues += [item]
            }
        }
        return uniqueValues
    }
}

enum CurrencyType: String, Codable {
    case EUR
    case USD
    case GBP
    case JPY

    func getSymbol() -> String {
        switch self {
        case .EUR:
            return "€"
        case .USD:
            return "$"
        case .GBP:
            return "£"
        case .JPY:
            return "¥"
        }
    }

    func getCode() -> String {
        switch self {
        case .EUR:
            return "es-ES"
        case .USD:
            return "en-US"
        case .GBP:
            return "en-GB"
        case .JPY:
            return "ja-JP"
        }
    }
}

enum FormatType {
    case decimal
    case percentage
    case currency(type: CurrencyType)
}

extension Double {
    func formatWithComma(decimals: Int = 2, type: FormatType, useSymbol: Bool = false) -> String {
        let numberFormatter = NumberFormatter()
        var value = self

        switch type {
        case .decimal:
            numberFormatter.numberStyle = NumberFormatter.Style.decimal
        case .percentage:
            value = self/100
            numberFormatter.numberStyle = NumberFormatter.Style.percent
        case .currency(let type):
            numberFormatter.numberStyle = NumberFormatter.Style.currency
            numberFormatter.locale = Locale(identifier: type.getCode())
        }

        numberFormatter.decimalSeparator = ","
        numberFormatter.groupingSeparator = "."
        numberFormatter.maximumFractionDigits = decimals
        numberFormatter.minimumFractionDigits = decimals < 2 ? decimals : 2
        let formattedNumber = numberFormatter.string(from: NSNumber(value: value))

        let symbolToUse = (useSymbol && value != 0) ? (value > 0 ? "+" : "-") : ""

        return formattedNumber != nil ? "\(symbolToUse)\(formattedNumber!)" : ""
    }
}

extension UIImage {

    func imageResize (sizeChange: CGSize) -> UIImage {

        let hasAlpha = true
        let scale: CGFloat = 0.0 // Use scale factor of main screen

        UIGraphicsBeginImageContextWithOptions(sizeChange, !hasAlpha, scale)
        self.draw(in: CGRect(origin: CGPoint.zero, size: sizeChange))

        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        return scaledImage!
    }
}

extension UIView {
    func layerGradient(alphaEnd : CGFloat, alphaStart : CGFloat?, color : UIColor){
        //THE CLEAREST (MORE TRANSPARENT) ON VIEW TOP

        let layer : CAGradientLayer = CAGradientLayer()
        layer.frame.size = self.frame.size
        layer.masksToBounds = true
        layer.frame.origin = CGPoint(x : 0.0, y : 0.0)

        let alphaTransition : CGFloat = 0.06

        let numberOfSteps = (alphaStart != nil)
            ? Int(floor((alphaEnd-alphaStart!)/alphaTransition))
            : Int(floor(alphaEnd/alphaTransition))

        layer.colors = []
        layer.locations = []

        if numberOfSteps <= 0{
            return
        }

        for i in 0...numberOfSteps{

            let stepAlpha = (alphaStart != nil)
                ? (CGFloat(numberOfSteps)*alphaTransition+alphaStart!)-(CGFloat(i)*alphaTransition)
                : (CGFloat(numberOfSteps)*alphaTransition)-(CGFloat(i)*alphaTransition)

            layer.locations!.append((CGFloat(i) * (CGFloat(1) / CGFloat(numberOfSteps))) as NSNumber)
            layer.colors!.insert(color.withAlphaComponent(stepAlpha).cgColor, at: 0)
            //            layer.colors!.insert(UIColor(red:255/255, green:255.0/255, blue:255.0/255, alpha : stepAlpha).cgColor, at: 0)
        }

        self.layer.sublayers = nil
        self.layer.addSublayer(layer)
    }
}
