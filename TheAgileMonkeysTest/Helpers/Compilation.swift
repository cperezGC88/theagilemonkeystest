//
//  Compilation.swift
//  TheAgileMonkeysTest
//
//  Created by Carlos Perez on 11/04/2019.
//  Copyright © 2019 Carlos Perez. All rights reserved.
//

struct Compilation {
    static var environment: Environment = .mock
    
    static func getRandomImageURL() -> String {
        let images = ["https://www.apparentia.com/views/resources/img/productos/14605/l/blusa-de-fiesta-blanca-cuello-redondo-con-manga-con-volantes-para-invitadas-de-boda-eventos-comuniones-bautizos-apparentia.jpg",
                      "https://www.apparentia.com/views/resources/img/productos/14605/l/falda-coral-cruzada-y-top-azul-marino-brisa-volantes-vestir-invitada-fiesta-evento-boda-apparentia.jpg",
                      "https://www.apparentia.com/views/resources/img/productos/1658/l/mono-de-fiesta-calista-verde-espalda-cruzada-nochevieja-boda-apparentia-collection.jpg",
                      "https://www.apparentia.com/views/resources/img/productos/12784/l/pantalon-palazzo-frambuesa-pata-ancha-pajaros-apparentia.jpg",
                      "https://www.apparentia.com/views/resources/img/productos/15224/l/bolso-clutch-de-fiesta-dorado-con-pasamaneria-de-piedras-multicolor-apparentia.jpg",
                      "https://www.apparentia.com/views/resources/img/productos/14689/l/top-asimetrico-rayas-lazo-grande-para-invitadas-boda-bautizo-comunion-coctel-graduacion-vestidos-de-fiestaonline-apparentia-collection-primavera-verano-2017-shoponline.jpg",
                      "https://www.apparentia.com/views/resources/img/productos/14973/l/apparentia-collection-invitadas-vestidos-de-fiesta-bodas-invierno-abrigos-estola-pelo-rayas-blanco-y-negro.jpg",
                      "https://www.apparentia.com/views/resources/img/productos/15362/l/vestido-vuelo-antelina-burdeos-detalle-pajaros-colibri-colores-ideal-looks-apparentia7087.jpg",
                      "https://www.apparentia.com/views/resources/img/productos/14691/l/top-vaquero-de-tirantes-con-escote-corazon-y-peplum-para-invitadas-boda-fiesta-graduacion-bautizo-comunion-coctel-apparentioa-collection-on-line4930.jpg",
                      "https://www.apparentia.com/views/resources/img/productos/2639/l/bolso-de-fiesta-de-pluma-roja-de-marbu-con-cadena-dorada-adornando-el-bolso-de-apparentia-collection-invitadas-boda-comprar-online.jpg",
                      "https://www.apparentia.com/views/resources/img/productos/15334/l/pantalon-metalizados-oro-rosa-tostado-peplum-asimetrico-pitillo-apparentia-invitada.jpg",
                      "https://www.apparentia.com/views/resources/img/productos/2139/l/mono-jumpsuit-marsala-de-fiesta-de-manga-larga-pernera-ancha-escote-en-v-para-eventos-boda-coctel-bautizo-comunion-de-apparentia.jpg",
                      "https://www.apparentia.com/views/resources/img/productos/14617/l/comprar-online-falda-midi-azul-turquesa-saten-tornasol-para-invitada-de-boda-bautizo-comunion-eventos-fiesta-de-quince-graduacion-aniversario-de-apparentia-collection.jpg",
                      "https://www.apparentia.com/views/resources/img/productos/15045/l/apparentia-collection-invitadas-vestidos-de-fiesta-bodas-bautizo-comunion-eventos-vestido-esmoquin-lila-lavanda-volante-2.jpg"]

        return images[Int.random(in: 0 ..< images.count)]
    }
}
