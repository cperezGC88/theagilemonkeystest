import Foundation

class DownloadProgressDelegate : NSObject, URLSessionDownloadDelegate {

    var resourceDownloadDelegateProtocol : ResourceDownloaderDelegateProtocol?
    var action : ((String) -> Void)?
    var callbackParams : AnyObject?
    var destUrl : NSURL?

    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {

        guard let destinationUrl = destUrl else { return }

        let fileManager = FileManager()

        do {
            try fileManager.removeItem(at: destinationUrl as URL) //delete the file if exists
        } catch _ {
        }

        do {
            try fileManager.copyItem(at: location as URL, to: destinationUrl as URL) //make the copy
        } catch _ {

        }

        //check if the file exists and return the value
        let fileExists = fileManager.fileExists(atPath: destinationUrl.path!)

        DispatchQueue.main.async {
            if let callback = self.resourceDownloadDelegateProtocol {
                ResourceDownloader.removeFromDownloadingResources(urlPath: destinationUrl.path!)
                callback.resourceDownloaded(success: fileExists, destUrl: destinationUrl.path, param: self.callbackParams)
            }

            if let action = self.action {
                action(destinationUrl.path ?? "")
            }
        }
    }

    func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
        if let err = error,
            err._code == NSURLErrorTimedOut,
            let destinationUrl = self.destUrl,
            let callback = self.resourceDownloadDelegateProtocol {

            ResourceDownloader.removeFromDownloadingResources(urlPath: destinationUrl.path!)
            callback.resourceDownloadFailed(destUrl: destinationUrl.path!, param: self.callbackParams)
        }
    }

    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64) {
        DispatchQueue.main.async {
            if let destinationUrl = self.destUrl{
                if let callback = self.resourceDownloadDelegateProtocol {
                    callback.resourceDownloadProgress(destUrl: destinationUrl.path!, param: self.callbackParams, progress: Double(360*totalBytesWritten/totalBytesExpectedToWrite))
                }
            }
        }
    }

}
