import Foundation

public protocol ResourceDownloaderDelegateProtocol {
    func resourceDownloaded(success: Bool, destUrl: String?, param: AnyObject?)
    func resourceDownloadProgress(destUrl: String?, param: AnyObject?, progress : Double)
    func resourceDownloadFailed(destUrl: String?, param: AnyObject?)
}

public enum ResourceType {
    case video
    case image
    case audio
    case pdf

    func getExtension() -> String {
        switch self {
        case .video:
            return ".mp4"
        case .image:
            return ".jpeg"
        case .audio:
            return ".m4a"
        case .pdf:
            return ".pdf"
        }
    }
}

public class ResourceDownloader: NSObject {

    private static var downloadingResources = Set<String>()

    public class func downloadResource(url: String, resourceType: ResourceType, useCache: Bool, callback: ResourceDownloaderDelegateProtocol?, param: AnyObject?) -> String? {

        if let cachedResource = resourceAvailable(url: url, resourceType: resourceType), useCache {
            return cachedResource
        }

        let destUrl = cachedFileLocalPath(url: url, resourceType: resourceType)

        guard let path = destUrl?.path else { return nil }

        downloadingResources.insert(path)

        let backgroundQueue = DispatchQueue.global(qos: .background)

        //background proccess
        backgroundQueue.async(){
            //download to a file

            //create the request
            let nsUrl = NSURL(string: url)

            let delegate = DownloadProgressDelegate()
            delegate.resourceDownloadDelegateProtocol = callback
            delegate.destUrl = destUrl
            delegate.callbackParams = param

            let session = URLSession(configuration: URLSessionConfiguration.default, delegate: delegate, delegateQueue: nil)

            if let validUrl = nsUrl{
                let downloadTask = session.downloadTask(with: validUrl as URL)
                downloadTask.resume()
            }
        }

        //no cached result, return nil
        return nil
    }

    public class func removeFromDownloadingResources(urlPath : String){
        downloadingResources.remove(urlPath)
    }

    private class func cachedFileLocalPath(url: String, resourceType: ResourceType) -> NSURL! {
        //cache dirs
        var paths = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.cachesDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
        let cacheFolder = paths[0]

        guard var urlMD5 = url.md5() else { return nil }
        urlMD5 += resourceType.getExtension()

        return NSURL.fileURL(withPathComponents: [cacheFolder, urlMD5])! as NSURL
    }

    public class func resourceAvailable(url: String, resourceType: ResourceType) -> String? {
        let fileFullPath = self.cachedFileLocalPath(url: url, resourceType: resourceType).path!
        let fileManager = FileManager()

        //check if the file exists
        if fileManager.fileExists(atPath: fileFullPath){
            return fileFullPath

        } else {
            return nil
        }
    }

    public class func invalidateResource(url: String) {
        let fileManager = FileManager()

        do{
            try fileManager.removeItem(atPath: url)
        } catch _ {

        }
    }

    public class func isDownloading(url: String, resourceType: ResourceType) -> Bool {

        let destUrl = cachedFileLocalPath(url: url, resourceType: resourceType)
        var result = false

        if let destUrl = destUrl {
            if downloadingResources.contains(destUrl.path!){
                result = true
            }
        }

        return result
    }

    public class func moveDownloadedResourceToCache(localFile: String, remoteFile: String, resourceType: ResourceType) -> Bool {
        let fileManager = FileManager()

        if !fileManager.fileExists(atPath: localFile){
            return false
        }

        var result = true
        guard let destUrl = self.cachedFileLocalPath(url: remoteFile, resourceType: resourceType) else { return false }

        do {
            try fileManager.removeItem(at: destUrl as URL) //delete the final file if exists
        } catch _ {
        }

        do {
            try fileManager.moveItem(atPath: localFile, toPath: destUrl.path!)
        } catch _ {
            result = false
        }

        return result
    }

    public class func clearCache(type: ResourceType?) -> Bool {
        var paths = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.cachesDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
        let cacheFolder = paths[0]
        let fileManager = FileManager()
        var filePaths = Array<String>()

        do{
            filePaths = try fileManager.contentsOfDirectory(atPath: cacheFolder)

        } catch _ {
            return false
        }

        if let type = type {
            filePaths = filePaths.filter({$0.contains(type.getExtension())})
        }

        for filePath in filePaths {

            if filePath == Bundle.main.bundleIdentifier! {
                do {
                    let finalPath = cacheFolder + "/" + filePath
                    try fileManager.removeItem(atPath: finalPath)

                } catch _ {
                    print("Cache could not clear \(filePath)")
                }
            }
        }

        return true
    }

}
