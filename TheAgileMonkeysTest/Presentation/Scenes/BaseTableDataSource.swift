import UIKit

protocol BaseTableDataSourceListener: class {
    func didSelectElement(row: TableViewModel)
}

class BaseTableDataSource: NSObject, UITableViewDelegate, UITableViewDataSource {

    internal var viewModels: TableViewContent!
    private weak var listener: BaseTableDataSourceListener?

    init(viewModels: TableViewContent, listener: BaseTableDataSourceListener?) {
        self.viewModels = viewModels
        self.listener = listener
    }

    public func setViewModels(viewModels: TableViewContent) {
        self.viewModels = viewModels
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModels.getSections().count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard viewModels.getSections().count > 0 else { return 0 }
        return viewModels.getSections()[section].getRows().count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let vm = viewModels.getSections()[indexPath.section].getRows()[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: getReuseId(for: vm), for: indexPath)
        vm.setup(cell: cell)

        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let element = viewModels.getSections()[indexPath.section].getRows()[indexPath.row]
        listener?.didSelectElement(row: element)
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let vm = viewModels.getSections()[section].header,
            let cell = tableView.dequeueReusableHeaderFooterView(withIdentifier: getReuseId(for: vm)) else {

                return nil
        }

        vm.setup(cell: cell)

        return cell
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }

    // ScrollView Delegate

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
    }

    //MARK: METHODS TO BE OVERRIDEN FOR SCALABILITY

    internal func getReuseId(for vm: TableViewModel) -> String {
        //TO BE OVERRIDEN
        return ""
    }
}
