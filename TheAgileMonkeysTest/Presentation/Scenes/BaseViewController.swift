import UIKit

protocol BaseViewProtocol {
    func printInteractorError(error: String)
    func showLoading(messages: [String], callback: (() -> Void)?)
    func hideLoading(callback: (() -> Void)?)
}

protocol BasePresenter {
    func viewDidLoad()
    func viewWillAppear()
}

class BaseViewController: UIViewController, BaseViewProtocol {

    //NILLABLE SO IT JUST WILL BE ALLOCATED WHEN FIRST USED THE LOADER
    var loader: UIActivityIndicatorView?

    override func viewDidLoad() {
    }

    // PROTOCOL METHODS

    func printInteractorError(error: String) {
        let alert = UIAlertController(title: NSLocalizedString("Error", comment: ""), message: error, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("Accept", comment: ""), style: .default, handler: {(alert) in
            self.dismiss(animated: true, completion: nil)
        }))
        self.present(alert, animated: true, completion: nil)
    }

    func showLoading(messages: [String], callback: (() -> Void)?) {
        view.isUserInteractionEnabled = false
        loader = UIActivityIndicatorView(style: .gray)
        UIApplication.shared.keyWindow?.addSubview(loader!)

        loader?.translatesAutoresizingMaskIntoConstraints = false
        loader?.widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.width).isActive = true
        loader?.heightAnchor.constraint(equalToConstant: UIScreen.main.bounds.height).isActive = true
        loader?.leadingAnchor.constraint(equalTo: UIApplication.shared.keyWindow!.leadingAnchor, constant: 0).isActive = true
        loader?.topAnchor.constraint(equalTo: UIApplication.shared.keyWindow!.topAnchor, constant: 0).isActive = true

        loader?.isHidden = false
        loader?.startAnimating()
    }

    func hideLoading(callback: (() -> Void)?) {
        view.isUserInteractionEnabled = true
        loader?.removeFromSuperview()
    }
}
