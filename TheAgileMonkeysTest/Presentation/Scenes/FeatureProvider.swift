import UIKit

//FEATURE PROVIDER IS THE ONE IN CHARGE OF THE CREATION OF OUR VIPR CLASSES, KEEPING AN EYE ON THE
//DEPENDENCIES BETWEEN EACH PART

class FeatureProvider {

    private let dataSource: DataSourceProtocol

    public init(dataSource: DataSourceProtocol) {
        self.dataSource = dataSource
    }

    private func getStoryboard(name: String) -> UIStoryboard? {
        return UIStoryboard(name: name, bundle: Bundle.main)
    }

    func getStoresFeature() -> StoresViewController {

        guard let vc: StoresViewController = getStoryboard(name: "Main")?
            .instantiateViewController(withIdentifier: "StoresViewController") as? StoresViewController else {
                fatalError("UIVIEWCONTROLLER NOT FOUND")
        }
        let interactor = StoresInteractor(dataSource: dataSource as! StoresDataSource)
        let router = StoresRouter(featureProvider: self, view: vc)
        let presenter = StoresPresenter(view: vc, interactor: interactor, router: router)
        vc.presenter = presenter

        return vc
    }

    func getProductsFeature(storeId: Int) -> ProductsViewController {

        guard let vc: ProductsViewController = getStoryboard(name: "Main")?
            .instantiateViewController(withIdentifier: "ProductsViewController") as? ProductsViewController else {
                fatalError("UIVIEWCONTROLLER NOT FOUND")
        }
        let interactor = ProductsInteractor(dataSource: dataSource as! ProductsDataSource)
        let router = ProductsRouter(featureProvider: self, view: vc)
        let presenter = ProductsPresenter(storeId: storeId, view: vc, interactor: interactor, router: router)
        vc.presenter = presenter

        return vc
    }

}
