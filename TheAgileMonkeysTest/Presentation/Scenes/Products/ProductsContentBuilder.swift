//
//  ProductsContentBuilder.swift
//  TheAgileMonkeysTest
//
//  Created by Carlos Perez on 09/04/2019.
//  Copyright © 2019 Carlos Perez. All rights reserved.
//

import Foundation

class ProductsContentBuilder {

    private let products: [ProductDO]
    private let categories: [CategoryDO]

    init(products: [ProductDO], categories: [CategoryDO]) {
        self.products = products
        self.categories = categories
    }

    func getProducts() -> TableViewContent {
        let content = TableViewContent()

        let uniqueCategories = self.categories.unique
        uniqueCategories.forEach({category in
            let section = TableViewSection()
            section.header = CategoryViewModel(model: category)

            let categoryProducts = self.products.filter({$0.categoryId == Int(category.categoryId ?? "0")})
            categoryProducts.forEach({product in
                let row = ProductViewModel(model: product)
                section.addRow(row: row)
            })

            content.addSection(section: section)
        })

        return content
    }

}
