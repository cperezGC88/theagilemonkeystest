import Foundation

class ProductsInteractor: ProductsInteractorProtocol {

    private let dataSource: ProductsDataSource

    public init(dataSource: ProductsDataSource) {
        self.dataSource = dataSource
    }

    func getProducts(storeId: Int,
                     categoryId: [String],
                     needsLoader: ((Bool) -> Void)?,
                     completion: @escaping ([ProductDO]) -> Void,
                     failure: @escaping (String?) -> Void) {

        guard categoryId.count > 0 else {
            completion([])
            return
        }

        var output = [ProductDO]()
        let group = DispatchGroup()

        DispatchQueue.global(qos: .background).async {
            for category in categoryId {
                group.enter()

                self.dataSource.getProducts(storeId: storeId,
                                       filters: nil,
                                       with_text: nil,
                                       category_id: category,
                                       order: nil,
                                       page: nil,
                                       limit: nil,
                                       needsLoader: needsLoader,
                                       completion: {(result) in

                        let res: [ProductDO] = result?.first?.results?.map({(product) in
                            let prodDO = ProductDO(dto: product)
                            prodDO.categoryId = Int(category)
                            return prodDO
                        }) ?? []

                        output.append(contentsOf: res)
                        group.leave()

                }, failure: {(error) in
                    group.leave()
                })
            }

            group.notify(queue: .main) {
                completion(output)
            }
        }
    }

    func getCategories(storeId: Int,
                       needsLoader: ((Bool) -> Void)?,
                       completion: @escaping ([CategoryDO]) -> Void,
                       failure: @escaping (String?) -> Void) {

        dataSource.getCategories(storeId: storeId,
                                 needsLoader: needsLoader,
                                 completion: {(result) in

                                    completion(result?.map({CategoryDO(dto: $0)}) ?? [])
        }, failure: failure)
    }

}
