import Foundation

protocol ProductsInteractorProtocol {
    func getProducts(storeId: Int,
                     categoryId: [String],
                     needsLoader: ((Bool) -> Void)?,
                     completion: @escaping ([ProductDO]) -> Void,
                     failure: @escaping (String?) -> Void)

    func getCategories(storeId: Int,
                       needsLoader: ((Bool) -> Void)?,
                       completion: @escaping ([CategoryDO]) -> Void,
                       failure: @escaping (String?) -> Void)
}

protocol ProductsViewProtocol: class, BaseViewProtocol {
    func setTableView(newViewModels: TableViewContent)
}

protocol ProductsRouterProtocol {
}

class ProductsPresenter: ProductsPresenterProtocol {

    weak var viewController: ProductsViewProtocol?
    let interactor: ProductsInteractorProtocol
    let router: ProductsRouterProtocol

    var sections = TableViewContent()
    let storeId: Int

    init(storeId: Int,
         view: ProductsViewProtocol,
         interactor: ProductsInteractorProtocol,
         router: ProductsRouterProtocol) {

        self.storeId = storeId
        self.viewController = view
        self.interactor = interactor
        self.router = router
    }

    func viewDidLoad() {
        interactor.getCategories(storeId: storeId,
                                 needsLoader: {needs in

                                    self.viewController?.showLoading(messages: [NSLocalizedString("please_wait", comment: "")], callback: nil)
        }, completion: {categories in

            let categoryIds: [String] = categories.compactMap({$0.categoryId})
            self.interactor.getProducts(storeId: self.storeId,
                                        categoryId: categoryIds,
                                        needsLoader: nil,
                                        completion: {(products) in

                                            self.onGetProductsSuccess(products: products, categories: categories)

            }, failure: {(error) in
                self.onGetProductsFailure(error: error)
            })

        }, failure: {error in
            self.onGetProductsFailure(error: error)
        })
    }

    func viewWillAppear() {
    }

    // Mark: Private Methods

    private func onGetProductsSuccess(products: [ProductDO], categories: [CategoryDO]) {
        let content = ProductsContentBuilder(products: products, categories: categories).getProducts()
        self.sections = content
        self.viewController?.setTableView(newViewModels: self.sections)

        self.viewController?.hideLoading(callback: nil)
    }

    private func onGetProductsFailure(error: String?) {
        self.viewController?.hideLoading(callback: nil)
        self.viewController?.printInteractorError(error: NSLocalizedString("there_was_error", comment: ""))
    }
}
