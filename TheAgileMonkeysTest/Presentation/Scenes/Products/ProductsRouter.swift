import UIKit

class ProductsRouter: NSObject, ProductsRouterProtocol {

    let featureProvider: FeatureProvider
    weak var view: UIViewController?

    init(featureProvider: FeatureProvider, view: UIViewController) {
        self.featureProvider = featureProvider
        self.view = view
    }
}
