import UIKit

class ProductsTableDataSource: BaseTableDataSource {

    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

        let vm = viewModels.getSections()[indexPath.section].getRows()[indexPath.row]
        switch vm.self {
        case is ProductViewModel:
            return 150
        default:
            return UITableView.automaticDimension
        }
    }

    internal override func getReuseId(for vm: TableViewModel) -> String {

        switch vm.self {
        case is ProductViewModel:
            return ProductCellView.reuseId
        case is CategoryViewModel:
            return CategoryCellView.reuseId
        default:
            return ""
        }
    }

    // ScrollView Delegate

    override func scrollViewDidScroll(_ scrollView: UIScrollView) {

        guard let tableView = scrollView as? UITableView,
            let visibles = tableView.indexPathsForVisibleRows,
            visibles.count > 0 else { return }

        let offsetY = scrollView.contentOffset.y
        for cell in tableView.visibleCells {

            if let prodCell = cell as? ProductCellView,
                let indexPath = tableView.indexPath(for: cell) {

                let cellOffset = tableView.rectForRow(at: indexPath)

                let x = prodCell.userPicture.frame.origin.x
                let w = prodCell.userPicture.bounds.width
                let h = prodCell.userPicture.bounds.height
                let y = ((offsetY - cellOffset.origin.y - prodCell.userPicture.frame.origin.y) / h) * 15
                prodCell.userPicture.frame = CGRect(x: x, y: y, width: w, height: h)
            }
        }
    }
}
