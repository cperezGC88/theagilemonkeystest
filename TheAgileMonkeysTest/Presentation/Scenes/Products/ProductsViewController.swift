import UIKit

protocol ProductsPresenterProtocol: BasePresenter {
}

class ProductsViewController: BaseViewController, ProductsViewProtocol {

    var presenter: ProductsPresenterProtocol!

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var emptyView: UIView!
    @IBOutlet weak var emptyLabel: UILabel!

    private var tableDelegate: ProductsTableDataSource!

    override func viewDidLoad() {
        super.viewDidLoad()

        tableDelegate = ProductsTableDataSource(viewModels: TableViewContent(), listener: nil)
        tableView.delegate = tableDelegate
        tableView.dataSource = tableDelegate
        tableView.register(UINib(nibName: ProductCellView.reuseId, bundle: Bundle.main), forCellReuseIdentifier: ProductCellView.reuseId)
        tableView.register(UINib(nibName: CategoryCellView.reuseId, bundle: Bundle.main), forHeaderFooterViewReuseIdentifier: CategoryCellView.reuseId)
        tableView.alwaysBounceVertical = false
        tableView.rowHeight = UITableView.automaticDimension

        emptyView.isHidden = true
        emptyLabel.text = NSLocalizedString("no_store_found", comment: "")

        presenter.viewDidLoad()
    }

    // HOMEVIEWPROTOCOL METHODS

    func setTableView(newViewModels: TableViewContent) {

        guard newViewModels.getSections().count > 0 else {
            emptyView.isHidden = false
            return
        }

        emptyView.isHidden = true

        self.tableDelegate.setViewModels(viewModels: newViewModels)

        tableView.performTableUpdates(rowsToRemove: newViewModels.getIndexesToRemove(),
                                      sectionsToRemove: newViewModels.getSectionsToRemove(),
                                      rowsToInsert: newViewModels.getIndexesToInsert(),
                                      sectionsToInsert: newViewModels.getSectionsToInsert(),
                                      rowsToUpdate: newViewModels.getIndexesToUpdate(),
                                      animationMode: .fade)

        newViewModels.didReload()
    }

}
