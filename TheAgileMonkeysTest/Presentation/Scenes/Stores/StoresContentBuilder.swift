//
//  StoresContentBuilder.swift
//  TheAgileMonkeysTest
//
//  Created by Carlos Perez on 09/04/2019.
//  Copyright © 2019 Carlos Perez. All rights reserved.
//

import Foundation

class StoresContentBuilder {

    private let stores: [StoreDO]

    init(stores: [StoreDO]) {
        self.stores = stores
    }

    func getStores(headerListener: TableViewHeaderDelegate? = nil) -> TableViewContent {
        let content = TableViewContent()

        stores.forEach({(store) in
            
            let section = TableViewSection()
            section.header = CountryViewModel(model: store, headerListener: headerListener)
            store.storeViews?.forEach({storeView in
                let row = StoreViewModel(model: storeView)
                section.addRow(row: row)
            })

            content.addSection(section: section)
        })

        return content
    }

}
