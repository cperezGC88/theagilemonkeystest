import Foundation

class StoresInteractor: StoresInteractorProtocol {

    private let dataSource: StoresDataSource

    public init(dataSource: StoresDataSource) {
        self.dataSource = dataSource
    }

    func getStores(needsLoader: ((Bool) -> Void)?,
                    completion: @escaping ([StoreDO]) -> Void,
                    failure: @escaping (String?) -> Void) {

        dataSource.getStores(needsLoader: needsLoader,
                             completion: {(result) in
                                
            completion(result?.map({StoreDO(dto: $0)}) ?? [])
        }, failure: failure)
    }

}
