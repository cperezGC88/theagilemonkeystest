import Foundation

protocol StoresInteractorProtocol {
    func getStores(needsLoader: ((Bool) -> Void)?,
                   completion: @escaping ([StoreDO]) -> Void,
                   failure: @escaping (String?) -> Void)
}

protocol StoresViewProtocol: class, BaseViewProtocol {
    func setTableView(newViewModels: TableViewContent)
}

protocol StoresRouterProtocol {
    func goStoreProducts(storeId: Int)
}

class StoresPresenter: StoresPresenterProtocol {

    weak var viewController: StoresViewProtocol?
    let interactor: StoresInteractorProtocol
    let router: StoresRouterProtocol

    var sections = TableViewContent()

    init(view: StoresViewProtocol,
         interactor: StoresInteractorProtocol,
         router: StoresRouterProtocol) {

        self.viewController = view
        self.interactor = interactor
        self.router = router
    }

    func viewDidLoad() {
        interactor.getStores(needsLoader: {needs in
            self.viewController?.showLoading(messages: [NSLocalizedString("please_wait", comment: "")], callback: nil)
        }, completion: {results in

            let content = StoresContentBuilder(stores: results).getStores(headerListener: self)
            self.sections = content
            self.viewController?.setTableView(newViewModels: self.sections)

            self.viewController?.hideLoading(callback: nil)

        }, failure: {error in
            self.viewController?.hideLoading(callback: nil)
            self.viewController?.printInteractorError(error: NSLocalizedString("there_was_error", comment: ""))
        })
    }

    func viewWillAppear() {
    }
}

extension StoresPresenter: BaseTableDataSourceListener {
    func didSelectElement(row: TableViewModel) {
        if let vm = row as? StoreViewModel {
            router.goStoreProducts(storeId: vm.model.storeId ?? 0)
        }
    }
}

extension StoresPresenter: TableViewHeaderDelegate {
    func didSelectHeader(vm: TableViewModel?) {
        if let section = vm?.section {
            section.isCollapsed = !section.isCollapsed
            viewController?.setTableView(newViewModels: sections)
        }
    }
}
