import UIKit

class StoresRouter: NSObject, StoresRouterProtocol {

    let featureProvider: FeatureProvider
    weak var view: UIViewController?

    init(featureProvider: FeatureProvider, view: UIViewController) {
        self.featureProvider = featureProvider
        self.view = view
    }

    func goStoreProducts(storeId: Int) {
        let vc = featureProvider.getProductsFeature(storeId: storeId)
        view?.navigationController?.pushViewController(vc, animated: true)
    }
}
