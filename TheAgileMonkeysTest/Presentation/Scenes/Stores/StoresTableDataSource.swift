import UIKit

class StoresTableDataSource: BaseTableDataSource {

    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }

    internal override func getReuseId(for vm: TableViewModel) -> String {

        switch vm.self {
        case is StoreViewModel:
            return StoreCellView.reuseId
        case is CountryViewModel:
            return CountryCellView.reuseId
        default:
            return ""
        }
    }
}
