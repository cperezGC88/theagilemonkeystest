import UIKit

protocol StoresPresenterProtocol: BasePresenter, BaseTableDataSourceListener {
}

class StoresViewController: BaseViewController, StoresViewProtocol {

    var presenter: StoresPresenterProtocol!

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var emptyView: UIView!
    @IBOutlet weak var emptyLabel: UILabel!

    private var tableDelegate: StoresTableDataSource!

    override func viewDidLoad() {
        super.viewDidLoad()

        tableDelegate = StoresTableDataSource(viewModels: TableViewContent(), listener: presenter)
        tableView.delegate = tableDelegate
        tableView.dataSource = tableDelegate
        tableView.register(UINib(nibName: StoreCellView.reuseId, bundle: Bundle.main), forCellReuseIdentifier: StoreCellView.reuseId)
        tableView.register(UINib(nibName: CountryCellView.reuseId, bundle: Bundle.main), forHeaderFooterViewReuseIdentifier: CountryCellView.reuseId)
        tableView.alwaysBounceVertical = false

        emptyView.isHidden = true
        emptyLabel.text = NSLocalizedString("no_store_found", comment: "")

        presenter.viewDidLoad()
    }

    // HOMEVIEWPROTOCOL METHODS

    func setTableView(newViewModels: TableViewContent) {

        guard newViewModels.getSections().count > 0 else {
            emptyView.isHidden = false
            return
        }

        emptyView.isHidden = true

        self.tableDelegate.setViewModels(viewModels: newViewModels)

        tableView.performTableUpdates(rowsToRemove: newViewModels.getIndexesToRemove(),
                                      sectionsToRemove: newViewModels.getSectionsToRemove(),
                                      rowsToInsert: newViewModels.getIndexesToInsert(),
                                      sectionsToInsert: newViewModels.getSectionsToInsert(),
                                      rowsToUpdate: newViewModels.getIndexesToUpdate(),
                                      animationMode: .fade)

        newViewModels.didReload()
    }

}
