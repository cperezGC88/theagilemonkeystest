protocol CountryCellViewProtocol {
    static var reuseId: String { get }
    var name: String? { get set }
}

class CountryViewModel: TableViewModel {

    public var name: String {
        didSet {
            section?.rowWasUpdated(row: self)
        }
    }

    public var model: StoreDO
    weak var headerListener: TableViewHeaderDelegate?

    weak var section: TableViewSection?

    init(model: StoreDO, headerListener: TableViewHeaderDelegate?) {
        self.model = model
        self.headerListener = headerListener

        self.name = model.name ?? ""
    }

    func setup(cell: Any) {
        guard var validCell = cell as? CountryCellViewProtocol else {
            print("NOT SUPPORTED CELL: \(cell)")
            return
        }

        if var header = cell as? TableViewHeader {
            header.delegate = self
        }

        validCell.name = name
    }

}

extension CountryViewModel: TableViewHeaderDelegate {
    func didSelectHeader(vm: TableViewModel?) {
        headerListener?.didSelectHeader(vm: self)
    }
}
