protocol ProductCellViewProtocol {
    static var reuseId: String { get }
    var name: String? { get set }
    var currency: CurrencyType? { get set }
    var price: Double? { get set }
    var images: [String]? { get set }
}

class ProductViewModel: TableViewModel {

    public var name: String {
        didSet {
            section?.rowWasUpdated(row: self)
        }
    }

    public var currency: CurrencyType {
        didSet {
            section?.rowWasUpdated(row: self)
        }
    }

    public var price: Double? {
        didSet {
            section?.rowWasUpdated(row: self)
        }
    }

    public var images: [String] {
        didSet {
            section?.rowWasUpdated(row: self)
        }
    }

    public var model: ProductDO

    weak var section: TableViewSection?

    init(model: ProductDO) {
        self.model = model

        self.name = model.name ?? ""
        self.images = model.images ?? []
        self.currency = model.currency ?? .EUR
        self.price = model.finalPrice != nil ? (Double(model.finalPrice!)/100) : nil
    }

    func setup(cell: Any) {
        guard var validCell = cell as? ProductCellViewProtocol else {
            print("NOT SUPPORTED CELL: \(cell)")
            return
        }

        validCell.name = name
        validCell.images = images
        validCell.currency = currency
        validCell.price = price
    }

}
