protocol StoreCellViewProtocol {
    static var reuseId: String { get }
    var name: String? { get set }
}

class StoreViewModel: TableViewModel {

    public var name: String {
        didSet {
            section?.rowWasUpdated(row: self)
        }
    }

    public var model: StoreViewDO

    weak var section: TableViewSection?

    init(model: StoreViewDO) {
        self.model = model

        self.name = model.name ?? ""
    }

    func setup(cell: Any) {
        guard var validCell = cell as? StoreCellViewProtocol else {
            print("NOT SUPPORTED CELL: \(cell)")
            return
        }

        validCell.name = name
    }

}
