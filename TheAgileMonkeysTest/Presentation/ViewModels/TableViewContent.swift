import Foundation

class TableViewContent {

    private var sections: [TableViewSection] = []

    private var sectionsToInsert = [Int]()
    private var sectionsToRemove = [Int]()
    private var sectionsToUpdate = [Int]()

    func describe() {
        print("\nsectionsToInsert:\n\(sectionsToInsert)")
        print("\nsectionsToRemove:\n\(sectionsToRemove)")
        print("\nsectionsToUpdate:\n\(sectionsToUpdate)")
        print("\ngetIndexesToInsert:\n\(getIndexesToInsert())")
        print("\ngetIndexesToRemove:\n\(getIndexesToRemove())")
        print("\ngetIndexesToUpdate:\n\(getIndexesToUpdate())")
    }

    func getSections() -> [TableViewSection] {
        return sections
    }

    func addSection(section: TableViewSection, index: Int? = nil) {
        if let index = index,
            index <= sections.count {

            sections.insert(section, at: index)
            sectionsToInsert.append(index)
        } else {
            sections.append(section)
            sectionsToInsert.append(sections.count - 1)
        }
    }

    func getSection(for row: TableViewModel) -> TableViewSection? {
        if let index = sections.firstIndex(where: {$0.getRows().contains(where: {$0 === row})}) {
            return sections[index]
        }

        return nil
    }

    func removeSection(section: TableViewSection?, index: Int? = nil) {
        if let index = index,
            index <= sections.count {

            sections.remove(at: index)
            sectionsToRemove.append(index)
        } else {
            if let section = section,
                let index = sections.firstIndex(where: {$0 === section}) {

                sections.remove(at: index)
                sectionsToRemove.append(index)
            }
        }
    }

    func setSections(newSections: [TableViewSection]) {
        if newSections.count > sections.count {
            if sections.count > 0 {
                sectionsToUpdate.append(contentsOf: 0 ... sections.count - 1)
            }
            sectionsToInsert.append(contentsOf: sections.count ... newSections.count - 1)
        } else {
            sectionsToUpdate.append(contentsOf: 0 ... (newSections.count - 1 > 0 ? newSections.count - 1 : 0))
            sectionsToRemove.append(contentsOf: newSections.count ... sections.count - 1)
        }

        self.sections = newSections
    }

    func didReload() {
        sectionsToInsert = [Int]()
        sectionsToRemove = [Int]()
        sectionsToUpdate = [Int]()

        sections.forEach({section in
            section.didReload()
        })
    }

    //SECTIONS

    func getSectionsToInsert() -> [Int] {
        return sectionsToInsert
    }

    func getSectionsToRemove() -> [Int] {
        return sectionsToRemove
    }

    func getSectionsToUpdate() -> [Int] {
        return sectionsToUpdate
    }

    //ROWS

    func getIndexesToInsert() -> [IndexPath] {
        var output = [IndexPath]()
        _ = sections.enumerated().map({ind, section in
            output.append(contentsOf: section.getIndexesToInsert(section: ind))
        })

        return output
    }

    func getIndexesToRemove() -> [IndexPath] {
        var output = [IndexPath]()
        _ = sections.enumerated().map({ind, section in
            output.append(contentsOf: section.getIndexesToRemove(section: ind))
        })

        return output
    }

    func getIndexesToUpdate() -> [IndexPath] {
        var output = [IndexPath]()
        _ = sections.enumerated().map({ind, section in
            output.append(contentsOf: section.getIndexesToUpdate(section: ind))
        })

        return output
    }

}
