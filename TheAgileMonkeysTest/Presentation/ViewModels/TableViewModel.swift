protocol TableViewModel: class {
    var section: TableViewSection? { get set }
    func setup(cell: Any)
}
