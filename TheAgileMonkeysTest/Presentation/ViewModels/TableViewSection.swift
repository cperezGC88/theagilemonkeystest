import Foundation

class TableViewSection {

    var header: TableViewModel? {
        didSet {
            header?.section = self
        }
    }

    private var rows: [TableViewModel] = []

    private var indexToInsert = [Int]()
    private var indexToRemove = [Int]()
    private var indexToUpdate = [Int]()

    var isCollapsed: Bool = false {
        didSet {
            if isCollapsed {
                indexToRemove = rows.enumerated().map({ind, elem in
                    return ind
                })
            } else {
                indexToInsert = rows.enumerated().map({ind, elem in
                    return ind
                })
            }
        }
    }

    func getRows() -> [TableViewModel] {
        return isCollapsed ? [] : rows
    }

    func addRow(row: TableViewModel, index: Int? = nil) {
        if let index = index,
            index <= rows.count {

            rows.insert(row, at: index)
            indexToInsert.append(index)
        } else {
            rows.append(row)
            indexToInsert.append(rows.count-1)
        }

        let newRow = row
        newRow.section = self
    }

    func removeRow(row: TableViewModel?, index: Int? = nil) {
        if let index = index,
            index <= rows.count {

            rows.remove(at: index)
            indexToRemove.append(index)
        } else {
            if let row = row,
                let index = self.rows.firstIndex(where: {$0 === row}) {

                rows.remove(at: index)
                indexToRemove.append(index)
            }
        }
    }

    func setRows(newRows: [TableViewModel]) {
        if newRows.count > rows.count {
            if rows.count > 0 {
                indexToUpdate.append(contentsOf: 0 ... rows.count - 1)
            }
            indexToInsert.append(contentsOf: rows.count ... newRows.count - 1)
        } else {
            indexToUpdate.append(contentsOf: 0 ... (newRows.count - 1 > 0 ? newRows.count - 1 : 0))
            indexToRemove.append(contentsOf: newRows.count ... rows.count - 1)
        }

        rows = newRows
        if rows.count > 0 {
            for i in 0 ... rows.count - 1 {
                rows[i].section = self
            }
        }
    }

    func rowWasUpdated(row: TableViewModel) {
        if let ind = rows.firstIndex(where: {$0 === row}),
            !indexToUpdate.contains(ind) {

            indexToUpdate.append(ind)
        }
    }

    func didReload() {
        indexToInsert = [Int]()
        indexToRemove = [Int]()
        indexToUpdate = [Int]()
    }

    func getIndexesToInsert(section: Int) -> [IndexPath] {
        return indexToInsert.enumerated().map({ind, elem in
            return IndexPath(row: elem, section: section)
        })
    }

    func getIndexesToRemove(section: Int) -> [IndexPath] {
        return indexToRemove.enumerated().map({ind, elem in
            return IndexPath(row: elem, section: section)
        })
    }

    func getIndexesToUpdate(section: Int) -> [IndexPath] {
        return indexToUpdate.enumerated().map({ind, elem in
            return IndexPath(row: elem, section: section)
        })
    }

}

protocol TableViewHeader {
    var delegate: TableViewHeaderDelegate? { get set }
}

protocol TableViewHeaderDelegate: class {
    func didSelectHeader(vm: TableViewModel?)
}
