import UIKit

class CategoryCellView: UITableViewHeaderFooterView, CategoryCellViewProtocol {

    @IBOutlet weak var nameLabel: UILabel!

    static var reuseId: String = "CategoryCellView"

    override var reuseIdentifier: String? {
        return "CategoryCellView"
    }

    var name: String? {
        didSet {
            nameLabel.text = name
        }
    }

    override func awakeFromNib() {
        contentView.backgroundColor = .lightGray
    }
}
