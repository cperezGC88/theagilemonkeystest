//
//  CountryCellView.swift
//  TheAgileMonkeysTest
//
//  Created by Carlos Perez on 10/04/2019.
//  Copyright © 2019 Carlos Perez. All rights reserved.
//

import UIKit

class CountryCellView: UITableViewHeaderFooterView, CountryCellViewProtocol, TableViewHeader {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var button: UIButton!

    weak var delegate: TableViewHeaderDelegate?

    static var reuseId: String = "CountryCellView"

    override var reuseIdentifier: String? {
        return "CountryCellView"
    }

    var name: String? {
        didSet {
            nameLabel.text = name
        }
    }

    override func awakeFromNib() {
        contentView.backgroundColor = .lightGray
        button.addTarget(self, action: #selector(CountryCellView.onHeaderTap), for: .touchUpInside)
    }

    @objc func onHeaderTap() {
        delegate?.didSelectHeader(vm: nil)
    }
}
