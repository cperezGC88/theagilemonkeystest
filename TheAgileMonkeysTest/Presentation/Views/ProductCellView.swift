//
//  ProductCellView.swift
//  TheAgileMonkeysTest
//
//  Created by Carlos Perez on 10/04/2019.
//  Copyright © 2019 Carlos Perez. All rights reserved.
//

import UIKit

class ProductCellView: UITableViewCell, ProductCellViewProtocol {

    @IBOutlet weak var userPicture: UIImageView!
    @IBOutlet weak var gradientView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!

    static var reuseId: String = "ProductCellView"

    override var reuseIdentifier: String? {
        return "ProductCellView"
    }

    var name: String? {
        didSet {
            nameLabel.text = name
        }
    }

    var currency: CurrencyType?

    var price: Double? {
        didSet {
            if let price = price {
                priceLabel.text = price.formatWithComma(decimals: 2, type: .currency(type: currency ?? .EUR), useSymbol: false)
            } else {
                priceLabel.text = NSLocalizedString("unknown", comment: "")
            }
        }
    }

    var images: [String]? {
        didSet {
            guard var validImageURL = images?.first else { return }
            validImageURL = Compilation.getRandomImageURL()
            let params = ["url": validImageURL]
            if let downloaded = ResourceDownloader.downloadResource(url: validImageURL, resourceType: .image, useCache: true, callback: self, param: params as AnyObject) {

                self.userPicture.image = UIImage(data: NSData(contentsOfFile: downloaded)! as Data)
            }
        }
    }

    override func awakeFromNib() {
        selectionStyle = .none
        userPicture.contentMode = .scaleAspectFill

        nameLabel.adjustsFontSizeToFitWidth = true
        nameLabel.minimumScaleFactor = 0.6
        nameLabel.textColor = .white
        priceLabel.adjustsFontSizeToFitWidth = true
        priceLabel.minimumScaleFactor = 0.6
        priceLabel.textColor = .white
    }

    override func prepareForReuse() {
        self.userPicture.image = nil
    }

    override func draw(_ rect: CGRect) {
        gradientView.layerGradient(alphaEnd: 0.4, alphaStart: nil, color: .black)
    }

}

extension ProductCellView: ResourceDownloaderDelegateProtocol {
    func resourceDownloaded(success: Bool, destUrl: String?, param: AnyObject?) {
        guard let url = destUrl,
            let dict = param as? [String: Any],
            let validUrl = dict["url"] as? String else { return }

        self.userPicture.image = UIImage(data: NSData(contentsOfFile: url)! as Data)
    }

    func resourceDownloadProgress(destUrl: String?, param: AnyObject?, progress: Double) {
    }

    func resourceDownloadFailed(destUrl: String?, param: AnyObject?) {
    }

}
