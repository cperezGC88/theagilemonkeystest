//
//  StoreCellView.swift
//  TheAgileMonkeysTest
//
//  Created by Carlos Perez on 09/04/2019.
//  Copyright © 2019 Carlos Perez. All rights reserved.
//

import UIKit

class StoreCellView: UITableViewCell, StoreCellViewProtocol {

    @IBOutlet weak var leftLabel: UILabel!
    @IBOutlet weak var rightLabel: UILabel!

    static var reuseId: String = "StoreCellView"

    override var reuseIdentifier: String? {
        return "StoreCellView"
    }

    var name: String? {
        didSet {
            rightLabel.text = name
        }
    }

}
