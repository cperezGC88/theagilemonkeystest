//
//  DataSource.swift
//  TheAgileMonkeysTest
//
//  Created by Carlos Perez on 09/04/2019.
//  Copyright © 2019 Carlos Perez. All rights reserved.
//

import Foundation

enum Environment {
    case mock
    case pre
}

protocol DataSourceProtocol {
}

class DataSource: DataSourceProtocol {

    internal let repositories: [Repository]
    internal let webServiceURLProvider: WebServiceURLProvider
    typealias T = Codable

    init(repositories: [Repository], webServiceURLProvider: WebServiceURLProvider) {
        self.repositories = repositories
        self.webServiceURLProvider = webServiceURLProvider
    }

    // Mark: Internal Methods

    internal func performRequest<T>(requestParams: Request,
                                   needsLoader: ((Bool) -> Void)?,
                                   completion: @escaping (([T]?) -> Void),
                                   failure: ((String?) -> Void)?) where T: Codable {

        getFromCacheIfNeeded(requestParams: requestParams,
                             completion: completion,
                             failure: {(error) in

                                self.requestFromRemote(requestParams: requestParams,
                                                       needsLoader: needsLoader,
                                                       completion: {(result) in

                                                        self.saveToCacheIfNeeded(result: result,
                                                                                 requestParams: requestParams,
                                                                                 completion: completion)
                                }, failure: failure)
        })
    }

    internal func getFromCacheIfNeeded<T>(requestParams: Request,
                                         completion: @escaping (([T]?) -> Void),
                                         failure: ((String?) -> Void)?) where T: Codable {

        if let cacheMethod = requestParams.shouldGetFromCache,
            let storage = repositories
                .filter({($0 as? StorageProtocol)?.storageType == cacheMethod})
                .first {

            //GET FROM CACHE
            storage.request(requestParams: requestParams,
                            completion: completion,
                            failure: failure)
        } else {
            failure?("CACHE_NOT_FOUND")
        }
    }

    internal func requestFromRemote<T>(requestParams: Request,
                                      needsLoader: ((Bool) -> Void)?,
                                      completion: @escaping (([T]?) -> Void),
                                      failure: ((String?) -> Void)?) where T: Codable {

        if let repository = repositories
            .filter({$0.repositoryType == requestParams.repositoryType})
            .first {

            needsLoader?(true)

            //GET FROM REMOTE
            repository.request(requestParams: requestParams,
                               completion: completion,
                               failure: failure)
        } else {
            failure?("REPO_NOT_FOUND")
        }
    }

    internal func saveToCacheIfNeeded<T>(result: [T]?,
                                        requestParams: Request,
                                        completion: @escaping (([T]?) -> Void)) where T: Codable {
        
        if let result = result,
            let cacheStorage = requestParams.shouldSaveInCache,
            let storage = self.repositories
                .filter({($0 as? StorageProtocol)?.storageType == cacheStorage})
                .first as? StorageProtocol {

            //SAVES TO CACHE
            storage.save(request: requestParams,
                         result: result)
        }

        completion(result)
    }

}
