//BY USING DATASOURCE EXTENSIONS, WE GET TO ISOLATE WHAT INTERACTORS CAN ACCESS AND ALSO
//TO GET ESCALABILITY BY NOT CREATING MASSIVE DATA SOURCES WITH THE WHOLE CALLS IN A SINGLE FILE

import Foundation

protocol ProductsDataSource {
    func getProducts(storeId: Int,
                     filters: String?,
                     with_text: String?,
                     category_id: String?,
                     order: String?,
                     page: String?,
                     limit: String?,
                     needsLoader: ((Bool) -> Void)?,
                     completion: @escaping (([ProductsResponse]?) -> Void),
                     failure: ((String?) -> Void)?)

    func getCategories(storeId: Int,
                       needsLoader: ((Bool) -> Void)?,
                       completion: @escaping (([Category]?) -> Void),
                       failure: ((String?) -> Void)?)
}

extension DataSource: ProductsDataSource {

    func getProducts(storeId: Int,
                     filters: String?,
                     with_text: String?,
                     category_id: String?,
                     order: String?,
                     page: String?,
                     limit: String?,
                     needsLoader: ((Bool) -> Void)?,
                     completion: @escaping (([ProductsResponse]?) -> Void),
                     failure: ((String?) -> Void)?) {

        let serviceUrl = webServiceURLProvider.productsURL
            .replacingOccurrences(of: "{{storeId}}", with: "\(storeId)")

        let params = ["filters": filters ?? "",
                      "with_text": with_text ?? "",
                      "category_id": category_id ?? "",
                      "order": order ?? "",
                      "page": page ?? "",
                      "limit": limit ?? ""]

        let request = Request(serviceUrl: serviceUrl,
                              params: params,
                              contentType: .queryString,
                              method: .get,
                              repositoryType: .rest,
                              shouldGetFromCache: nil,
                              shouldSaveInCache: nil,
                              validUntil: nil)

        performRequest(requestParams: request,
                       needsLoader: needsLoader,
                       completion: completion,
                       failure: failure)
    }

    func getCategories(storeId: Int,
                       needsLoader: ((Bool) -> Void)?,
                       completion: @escaping (([Category]?) -> Void),
                       failure: ((String?) -> Void)?) {

        let serviceUrl = webServiceURLProvider.categoriesURL
            .replacingOccurrences(of: "{{storeId}}", with: "\(storeId)")

        //CATEGORIES CACHED BY ONE HOUR
        let secondsValidData: Double = 60*60*24
        let deadLine = Date().addingTimeInterval(secondsValidData)

        let request = Request(serviceUrl: serviceUrl,
                              params: nil,
                              contentType: .json,
                              method: .get,
                              repositoryType: .rest,
                              shouldGetFromCache: .userDefaults,
                              shouldSaveInCache: .userDefaults,
                              validUntil: deadLine)

        performRequest(requestParams: request,
                       needsLoader: needsLoader,
                       completion: completion,
                       failure: failure)
    }
}
