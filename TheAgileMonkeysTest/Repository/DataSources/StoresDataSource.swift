import Foundation

protocol StoresDataSource {
    func getStores(needsLoader: ((Bool) -> Void)?,
                   completion: @escaping (([Store]?) -> Void),
                   failure: ((String?) -> Void)?)
}

extension DataSource: StoresDataSource {

    func getStores(needsLoader: ((Bool) -> Void)?,
                   completion: @escaping (([Store]?) -> Void),
                   failure: ((String?) -> Void)?) {

        let request = Request(serviceUrl: webServiceURLProvider.storesURL,
                              params: nil,
                              contentType: .json,
                              method: .get,
                              repositoryType: .rest,
                              shouldGetFromCache: nil,
                              shouldSaveInCache: nil,
                              validUntil: nil)

        performRequest(requestParams: request,
                       needsLoader: needsLoader,
                       completion: completion,
                       failure: failure)
    }
}
