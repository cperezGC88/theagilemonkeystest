//
//  Category.swift
//  Model Generated using http://www.jsoncafe.com/
//  Created on April 9, 2019

import Foundation

struct Category: Codable {

    let categoryId : String?
    let children : [Category]?
    let name : String?
}
