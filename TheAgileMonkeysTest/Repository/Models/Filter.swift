//
//  Filter.swift
//  Model Generated using http://www.jsoncafe.com/
//  Created on April 9, 2019

import Foundation

struct Filter : Codable {

    let currency : String?
    let filterName : String?
    let label : String?
    let max : Int?
    let min : Int?
    let options : [Option]?
    let type : String?
}
