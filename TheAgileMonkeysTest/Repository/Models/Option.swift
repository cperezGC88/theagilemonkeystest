//
//  Option.swift
//  Model Generated using http://www.jsoncafe.com/
//  Created on April 9, 2019

import Foundation

struct Option : Codable {

    let id : String?
    let imageUrl : String?
    let label : String?
}
