//
//  Result.swift
//  Model Generated using http://www.jsoncafe.com/
//  Created on April 9, 2019

import Foundation

struct Product : Codable {

    let care : String?
    let color : String?
    let composition : String?
    let currency : String?
    let descriptionField : String?
    let finalPrice : Int?
    let finalPriceType : String?
    let images : [String]?
    let modelId : String?
    let name : String?
    let originalPrice : Int?
    let sizes : [Size]?
    let sku : String?
    let type : String?
    let url : String?
}
