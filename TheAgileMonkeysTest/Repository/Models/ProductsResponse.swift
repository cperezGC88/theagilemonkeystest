//
//  Products.swift
//  Model Generated using http://www.jsoncafe.com/
//  Created on April 9, 2019

import Foundation

struct ProductsResponse : Codable {

    let filters : [Filter]?
    let results : [Product]?
    let resultsCount : Int?
}
