//
//  Size.swift
//  Model Generated using http://www.jsoncafe.com/
//  Created on April 9, 2019

import Foundation

struct Size : Codable {

    let name : String?
    let stockQty : Int?
    let variantId : String?
}
