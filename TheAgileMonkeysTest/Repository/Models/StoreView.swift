//
//  StoreView.swift
//  Model Generated using http://www.jsoncafe.com/
//  Created on April 9, 2019

import Foundation

struct StoreView : Codable {
    let name : String?
    let storeId : Int?
}
