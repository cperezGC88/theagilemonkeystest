//
//  Stores.swift
//  Model Generated using http://www.jsoncafe.com/
//  Created on April 9, 2019

struct Store : Codable {
    let countryCode : String?
    let name : String?
    let storeCode : String?
    let storeViews : [StoreView]?
    let websiteCode : String?
}
