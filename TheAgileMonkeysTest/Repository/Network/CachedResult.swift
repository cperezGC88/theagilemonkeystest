//THIS CLASS IS A KEY POINT FOR OUR LOCAL STORAGE METHODS. THIS CLASS GETS THE
//DATA WE NEED TO SAVE AND WRAPS IT INTO AN OBJECT THAT CAN BE SERIALIZED WITH THE
//CODABLE PROTOCOL HELP. THESE DATA WILL BE STORED WITH THE DATE IT WAS SAVED, SO
//IN CASE WE WANT TO GIVE THAT DATA A TIME TO LIVE, WE CAN JUST IGNORE IF TOO OLD

import Foundation

class CachedResult<T>: Codable where T: Codable {
    let creationDate: Date
    let validUntil: Date?
    let url: String
    let result: [T]

    init(creationDate: Date, validUntil: Date?, url: String, result: [T]) {
        self.creationDate = creationDate
        self.validUntil = validUntil
        self.url = url
        self.result = result
    }
}
