import Foundation

struct Request{
    public var serviceUrl: String?
    public var params: [String: Any]?
    public var contentType: RequestType
    public var method: RequestMethod?
    public var repositoryType: RepositoryType
    public var shouldGetFromCache: StorageType?
    public var shouldSaveInCache: StorageType?
    public var validUntil: Date?

    public init(serviceUrl: String? = nil,
                params: [String: Any]?,
                contentType: RequestType,
                method: RequestMethod?,
                repositoryType: RepositoryType,
                shouldGetFromCache: StorageType? = nil,
                shouldSaveInCache: StorageType? = nil,
                validUntil: Date? = nil) {

        self.serviceUrl = serviceUrl
        self.params = params
        self.contentType = contentType
        self.method = method
        self.repositoryType = repositoryType
        self.shouldGetFromCache = shouldGetFromCache
        self.shouldSaveInCache = shouldSaveInCache
        self.validUntil = validUntil
    }

    public func getId() -> String {
        var output = serviceUrl

        if let params = params, params.count > 0 {
            for (key, value) in params {
                output?.append(contentsOf: "_\(key)_\(value)")
            }
        }

        return output ?? ""
    }

    public func description() -> String {
        return "\(serviceUrl != nil ? "ServiceURL: \(serviceUrl!)\n" : "")" +
            "\(params != nil ? "Params: \(params!)\n" : "")" +
            "\(shouldGetFromCache != nil ? "FromCache: \(shouldGetFromCache!)\n" : "")" +
            "\(shouldSaveInCache != nil ? "SaveInCache: \(shouldSaveInCache!)\n" : "")"
    }
}

public enum RequestType: String {
    case urlEncoded
    case json
    case queryString

    case local
}

public enum RequestMethod: String {
    case get
    case post
    case delete
    case put
    case patch
}

public enum RepositoryType: String {
    case rest
    case soap
    case firebase

    case userDefaults
}

public enum StorageType: String {
    case userDefaults
    case internalMemory
    case keychain
}

protocol StorageProtocol {
    var storageType: StorageType { get }
    func save<T>(request: Request, result: [T]) where T: Codable
}
