//
//  UserDefaultsRepository.swift
//  TheAgileMonkeysTest
//
//  Created by Carlos Perez on 09/04/2019.
//  Copyright © 2019 Carlos Perez. All rights reserved.
//

import Foundation

class UserDefaultsRepository: Repository, StorageProtocol {

    var repositoryType: RepositoryType {
        return .userDefaults
    }

    var storageType: StorageType {
        return .userDefaults
    }

    let queue = OperationQueue()

    init() {
        queue.qualityOfService = .background
        queue.maxConcurrentOperationCount = 1
    }

    // Mark: Repository Methods

    func request<T>(requestParams: Request,
                    completion: @escaping ([T]) -> Void,
                    failure: ((String?) -> Void)?) where T: Codable {

        var output: CachedResult<T>? = nil

        let op = getOp<T>(request: requestParams)
        op.completionBlock = {() in
            output = op.output
            DispatchQueue.main.async {
                if let result = output {
                    print("GET FROM CACHE 👍: \n\(requestParams.description())")
                    completion(result.result)
                } else {
                    print("GET FROM CACHE 👎: \n\(requestParams.description())")
                    failure?("NOT_FOUND")
                }
            }
        }

        queue.addOperation(op)
    }

    // Mark: StorageProtocol Methods

    func save<T>(request: Request,
                 result: [T]) where T: Codable {

        queue.addOperation {
            let def = UserDefaults.standard
            do {
                let cachedResult = CachedResult(creationDate: Date(),
                                                validUntil: request.validUntil,
                                                url: request.getId(),
                                                result: result)

                let encoded = try JSONEncoder().encode(cachedResult).base64EncodedString()
                def.set(encoded, forKey: request.getId())
                print("CACHED 👍: \n\(request.description())")

            } catch _ {
                print("CACHED 👎: \n\(request.description())")
            }

            def.synchronize()
        }
    }

}

private class getOp<T>: Operation where T: Codable {

    var output: CachedResult<T>? = nil
    let request: Request

    init(request: Request) {
        self.request = request
    }

    override func main() {
        let def = UserDefaults.standard
        if let cached = def.string(forKey: request.getId()),
            let decoded = Data(base64Encoded: cached) {

            do {
                let recovered: CachedResult<T> = try JSONDecoder().decode(CachedResult<T>.self, from: decoded)
                output = recovered
                return
            } catch _ {

            }
        }

        return
    }
}
