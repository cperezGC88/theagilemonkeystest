//
//  WebServiceURLProvider.swift
//  TheAgileMonkeysTest
//
//  Created by Carlos Perez on 11/04/2019.
//  Copyright © 2019 Carlos Perez. All rights reserved.
//

protocol WebServiceURLProvider {
    var storesURL: String { get set }
    var categoriesURL: String { get set }
    var productsURL: String { get set }
}

class WebServiceURLProviderImpl: WebServiceURLProvider {
    var storesURL: String
    var categoriesURL: String
    var productsURL: String

    init(storesURL: String, categoriesURL: String, productsURL: String) {
        self.storesURL = storesURL
        self.categoriesURL = categoriesURL
        self.productsURL = productsURL
    }
}

class WebServiceURLProviderBuilder {

    private let storesURLMock = "https://private-anon-91ae1b785e-gocco.apiary-mock.com/stores"
    private let storesURLPre = "https://private-anon-91ae1b785e-gocco.apiary-proxy.com/stores"

    private let categoriesURLMock = "https://private-anon-91ae1b785e-gocco.apiary-mock.com/stores/{{storeId}}/categories"
    private let categoriesURLPre = "https://private-anon-91ae1b785e-gocco.apiary-proxy.com/stores/{{storeId}}/categories"

    private let productsURLMock = "https://private-anon-91ae1b785e-gocco.apiary-mock.com/stores/{{storeId}}/products/search"
    private let productsURLPre = "https://private-anon-91ae1b785e-gocco.apiary-proxy.com/stores/{{storeId}}/products/search"

    func getProvider(for environment: Environment) -> WebServiceURLProvider {
        switch Compilation.environment {
        case .mock:
            return WebServiceURLProviderImpl(storesURL: storesURLMock, categoriesURL: categoriesURLMock, productsURL: productsURLMock)
        case .pre:
            return WebServiceURLProviderImpl(storesURL: storesURLPre, categoriesURL: categoriesURLPre, productsURL: productsURLPre)
        }
    }
}
